# VIII - Offer and Beyond
## Handling Offers and Rejections
- Ask for extensions if needed when regarding deadlines
- Decline offers on good terms, keep line of communication open
    - Provide reason that is non-offensive, inarguable
- Rejection: thank recruiter for time, ask when to reapply for company
    - Possibly ask for feedback if able

### Offer Eval
#### Financial Package
* ![Financial Package](eval_final.png)
    - What i learn, and how a company advances career is critical
#### Career Dev
- Will be interviewing in a few years, important to think about hwo path impacts career path
    * ![Career Development](career_dev.png)
        - understand ^, less options in city mitigates great opportunities
#### Company Stability
- More stable companies are growing more slowly, amount of emphasis depends on if one can quickly find new job or not
#### Happiness Factor
* ![HF](HappyFactor.png)
    - Understand if you have synergy with company's product, did I enjoy talking with managers + teammates
    * How do future teammates enjoy company culture?
    * How many hours do future teammates work + hours before deadlines are longer

#### Negotiations
- Have a Viable Alternative: Communicate with recruiter about having other options in terms of joining
- A Specific Ask: Make sure to have an 'x' joining amount most likely scouting sources to understand avg. engineer salary then overshoot -> company will attempt to meet in middle
- Think Beyond Salary: If salary can't budge, consider asking for more equity, signing bonus or ask for relocation benefits (cash)
- Use Best Medium: Email or Phone

### On the Job
#### Set Timeline
- When enjoying job, easy to get wrapped up in it and realize there's no career advancement
- Outline career path: Where in 10 yrs? What are the req steps to get there? How career/skills have advanced and where to next?
#### Strong Relations
- Establish strong relations with managers, teammates for personal referrals
#### Ask
- Pursue challenges that are right for career, be transparent about goals w/ manager
#### Keep Interviewing
- Interview at least once a year to refine skills + keep in tune with opps, salaries out there // offers can allow for new connections with companies
