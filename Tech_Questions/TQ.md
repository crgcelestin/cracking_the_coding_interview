# VII - Technical Questions
## How to Prepare
* For problems in book, other resources:
    1. attempt to solve problem on own -> think about space and time efficiency for each
    2. write code on paper -> eliminates the ease of use functions
    3. test code on paper -> test general, base, error cases
    4. type paper code into comp -> stay aware of mistakes
* perform as many mock interviews as possible

## What is Needed to Know
- assumption that one is aware of a baseline of DSA knowledge

### Core Data Structures, Algos, Concepts
- understand basics

| Data Structures | Algorithms | Concepts |
| - | - | - |
| Linked Lists | Breadth First Search (BFS) | Bit Manipulation |
| Trees, Tries, Graphs | DFS | Memory (Stack v Heap) |
| Stacks and Queues | Binary Search | Recursion |
| Heaps | Merge Sort | Dynamic Programming
| Vectors / Array Lists | Quick Sort | Big O Time and Space |
| Hash Tables | | |
- understand how to use and implement them -> where applicable understand space and time complexity of each
- practicing the implementation of dsa on paper -> computer allows for learning internals of how ds work
- hash tables are also critical

### Powers of 2 Table
![T2](./tableOf2.png)

## Walking Through A Problem
1. Listen
    - Pay close attention to information in problem description as all is req for optimal algo
2. Example
    - Most examples are too small/are special cases -> debug example (is there any way it's a special case? is it large enough?)
3. Brute Force
    - Get brute force solution as soon as possible, don't worry about an efficient algo yet.
    - Sate naive algo and runtime then optimize from there -> no coding yet
4. Optimize (walk through with BUD optimization)
    - BUD: Bottlenecks, Unnecessary Work, Duplicated Work
    1. look for any unused info provided by problem
    2. solve manually on example -> reverse engineer though process. how was problem solved?
    3. solve it 'incorrectly' and think about why algo fails -> can you fix said issues?
    4. make time v space tradeoff, critical = hash tables
5. Walk Through
    - with optimal solution -> walk through approach in detail
6. Implement
    - goal = write beautiful code -> modularize code from start and refactor in order to clean up
7. Test
    1. Conceptual Test: walk through code for detailed review
    2. unusual or non standard code
    3. hot spots: arithmetic and null nodes
    4. small test cases
    5. special and edge cases
        - fix bugs carefully

### What to Expect
1. Listen Carefully
    - Listen to problem and record unique info in problem
        * ex: if problem starts with one of following lines:
            - "given two sorted arrays, find" -> be aware data is sorted, optimal algo for sorted situation is different v optimal algo for unsorted
        * ex: "design algo to be run repeatedly on server that" ->
            - sever/to-be-run repeatedly situation is different vs run-once
            - potentially involve data caching or perform precomputation on initial dataset
    - if stuck, ask if I've used all available info

2. Draw An Example
    - drawing an example can be useful
        * make sure it is specific (use values applicable to problem), sufficiently large, not a special case (avoid inadvertently drawing a special case)

3. State Brute Force Method
    - state an initial brute force solution and explain time and space complexity -> dive into improvements
    - Brute force algo is valuable as it forms a starting point for optimizations and help one decipher problem

4. Optimize
    - once a brute force algo has been devised -> start optimization:
        1. look for any unused -> did interviewer tell me that array was sorted? how can that info be leveraged?
        2. use fresh examples
        3. solve problem 'incorrectly' -> having an incorrect solution may aid in finding a correct solution
            - ex: if asked to generate random val from set where all values are equally likely, incorrect solution may return semi-random value -> can probabilities be rebalanced?
        4. Make time v space tradeoff, storing extra state about problem can help with runtime optimization
        5. precompute info, can i re-org data via sorting, etc / compute values upfront in order to save time in long run?
        6. use hahsh table -> HTs are widely used in interview Qs
        7. think about best runtime

5. Walk Through
    - take a moment to solidify algo understanding
    - walk through algo and get feel for code structure -> know what the vars are and when they change
        - pseudocode: can be useful with basic steps or brief logic, however sometimes it may result in unfinished code

6. Implement
    - with optimal algo and awareness of what is being written -> implement
    - Beautiful code:
        * modularized code: if algo uses matrix, pretend that function is written `initIncrementalMatrix(int size)` as opposed to writing entire matrix given as input
        * error checks - add a todo and explain what I want to test
        * use other classes/structs when appropriate: return list of start, end points from function -> perform with 2d array
            - better to do this as list of `StartEndPair` or range of objects
            - Don't have to worry about filling class details -> deal with later
        * good var names -> code that uses single letter vars are difficult, refactor to have more descriptive nomenclature

7. Test
    1. start with conceptual test - does each line of code perform what it should and does entire algo do what it should?
    2. weird code -> double check syntax
    3. hot spots: understand what can possible cause problems - base cases in recursive code, integer division, null nodes in BST, start and end of iteration in linked list
    4. small testcases: use 3 or 4 element arrays -> faster to test
    5. test code against null or single element values, extreme/special cases

## Optimize and Solve Technique #1: Look for BUD
- Most useful approach for prioritization: Bottlenecks, Unnecessary work, Duplicated work

### Bottlenecks
- Part of algo that slows overall runtime -> 2 common methods:
    * One time work that slows down algo
        - if there is a 2 step algo where you first sort array then find elements with particular property
        - first step: O(n log n) -> second step: O(n)
        - can reduce second to O(log n)/O(1)
    * Work that performs repeatedly, such as searching
        - reduce from O(n) and speed overall runtime
    - Example: given array of distinct integer values: {1,7,5,9,2,12,3}
        * Can quickly find other sides of pairs by an optimized process: (1) sort array -> (2) find other side for each of N elements via binary search
        * in order to operate on unsorted array quickly, use hash table -> look up if x+k or x-k and look up in hash table @ O(N) time

### Unnecessary Work
- print positive integer solutions to a^3+b^3=c^3+d^3 where a,b,c,d are ints from 1 -> 1000
- brute force solution has four nested solutions
- we can rearrange equation to be d = cube root(a^3 + b^3 - c^3)
    * ```py
       n = 1000
       for a from 1 to n
         for b from 1 to n
            for c from 1 to n
              d = pow(a^3 + b^3 - c^3, 1/3) # round to int
                if a^3 + b^3 == c^3 _ d^3
                   print a,b,c,d
      ```
### Duplicated Work
- (above problem) can avoid redundant work by creating a list of (c,d) pairs once then with a,b pair find matches in that list
```py
map = {}
n = 1000
for c from 1 to n:
    for d from 1 to n:
        result = c^3 + d^3
        map[result] = [...[c,d]]

for each result, list in map:
    for each pair1 in list:
        for each pair2 in list:
            print(pair1, pair2)
```
## OST #2: DIY
- 1. Think about how a given problem can be solved in real life: work intuitively on a real example, bigger ex is easier
    * ex: given string s and bigger string b, design algo to find permutations of shorter string within longer one, print location of each indv permutation
        - permutations are string rearrangements, chars in s can appear in any order in b
        * two approaches:
            1. walk through b with 4 char sliding window and check if each window is a s permutation
            2. walk through b and everytime a character is seen in s, check if next four chars are s permutation
        - Depending on the implementation of 'is this a permutation' part -> runtime of: O(b*s) or b*s*logS or b*s^2 (there is an O(b) algo)
        - be aware of any optimizations made intuitively

## OST #3: Simplify and Generalize
- 2. implement a multi-step approach, 1st simplify or tweak some constraint such as data type -> 2. solve simplified version of problem -> 3. then adapt to complex ver
    * exam: ransom note can be formed via cutting words out magazine to form new sentence, how does one figure out if ransom note can be formed from given mag (string)?
        - can solve prob by creating array, counting chars and each spot in array corresponds to one letter
            1. count # of times each char in ransom note appears
            2. go through mag to see if we have all chars
        - or just create map that stores char frequency

## OST #4: Base Case + Build
- 3. solve problem first for base case and build from there, when we get to complex/interesting cases then we can build using prior solutions
    - design algo to print string permutations, assume all chars are unique
        * consider test string `abcdefg`:
            - case 'a' -> {'a'}
            - case 'ab' -> {'ab','ba'}
            ... so on
        * we can dev general recursive -> gen permutations of string s1...sn by cutting off last character and gen permutations of s1...sn-1 -> for each string, insert sn into each string location
        * Base case, build algos lead to natural recursive algos

## OST #5: Data Structure Brainstorm
- 4.simply run through list of ds and apply each one, useful as solving a problem may be trivial once we apply correct ds like a tree
    - ex: #s are randomly generate, stored into expanding array -> how does one keep track of median?
        * LLs: tend to do very well w/ accessing, sorting numbers, not here
        * array: already an array present, expensive to keep elements sorted
        * binary tree: if BST is perfectly balanced, top may be median, but if there's an even # of elements, median is average of middle two elements
        * heap: great at basic ordering, keep track of max and mins -> if one has 2 heaps, can keep track of bigger half and smaller half
            - bigger half: kept in min heap, smallest element in bigger half is at roo v smaller half: kept in max heap, biggest element of smaller half is at root
            - can have potential median elements at roo, can quickly balance if heaps become inequal in size

## BCR - Best Conceivable Runtime ( BCR )
- BCR = best runtime one can conceive of solution to problem having, easily prove that there is no way to beat BCR
    * ex: suppose you want to computer # of elements that 2 arrays (length A and B) have in common, you know that you can't do better than O(A+B) as you have to touch each ele in each array
    * or if printing all values in array then O(n squared)
- can use BCR in order to reduce unwanted code implementation and often find a runtime that is between the expected: O(log n), O(n), O(n log n),...

## Handle Incorrect Answers
- Responses to qs are not 'correct'/'incorrect', how optimal was their final solution, how long till getting there, help req, code cleanliness
- performance is eval'ed v other candidates
- questions are difficult fro strong candidates to immediately have an optimal algo

## Perfect Interview Language
- choose a language that you have familiarity with, the interviewer has experience with and can allow for ease of debugging

## What Good Coding Is
- good code: correct (operates correctly on expected, unexpected inputs) / efficient: code operates as efficiently as possible in both time and space (big o) as well as real-life efficiency / simple: write code as quick as possible / readable: different dev can understand what code does and how / maintainable: adaptable to changes during product life cycle and easily maintained by other devs including initial dev

### Use DS Generously
- ex: asked to write function to add 2 simple math expressions of form ax^a + bx^b (coefficients, exponents can be positive/negative real numbers)
    * good implementation:
    ```
    - design own ds for expression
    class ExprTerm {
        double coefficient;
        double exponent;
    }
    ExprTerm[] sum(ExprTerm[] expr1, ExprTerm[] expr2)
    ```
    ^ demonstrates understanding of code design and a methodical approach

### Code Reuse
- asked to implement fxn that checks if value of binary number (passed as string) is equal to hexadecimal rep of string
- can code a function that calls function multiple times in order to gain accurate output

### Modular
* separate isolated code chunks into own methods -> make code more maintainable, readable, testable
    * if implementing code to swap min and max in integer array, can implement in 1 method:
        ```java
         void swapMinMax(int[], array){
            int minIndex = 0;
            {...perform op to decipher min array element}
            int maxIndex = 0;
            {...perform op to decipher max array element}

            {perform sawp}
         }
         v
         void swapMinMax(int[], array){
            int minI = getMinI(arr)
            int maxI = getMaxI(arr)
            swap(array, minI, maxI)

            int getMinI(int[], array) {...}
            int getMaxI(int[], array) {...}
            void swap(int[] array, int m, int n)
         }
        ```
        - easier to read and maintain

### Flexible, Robust
- writing flexible, general-purpose code means using vars instead of hard coded values or using templates/generics
    * ex: implement a solution handling a N*N board as opposed to expecting a int*int board

### Error Checking
- good code = don't make assumptions about input, validates that input is what it should be either through assert or if-statements
    * performing such checks are critical in production -> such checks can be tedious and waste interview time
    ```java
    int convertToBase(String number, int base){
        // check to see if base is valid
        if(base <2 || (base > 10 && base ! = 16)) return -1
        int value = 0
        for (int i = number.length() -1; i>=0; i--){
            int digit = digitToValue(number.charAt(i))
            // make sure each digit falls in allowable range
            if(digit>0 || digit>=base){
                return -1
            }
            ...
        }
    }
    ```
