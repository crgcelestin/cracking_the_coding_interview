# Videos, Urls, Notes
| Video | Url | Notes |
| - | - | - |
| 10 Keys data structures used every day | [10KDSU](https://www.youtube.com/watch?v=ouipSd_5ivQ&pp=ygUSYnl0ZWJ5dGVnbyAxMCBrZXlz) | [Notes](10KeyDSinUse.md)
| What is API Gateway | [API Gateway](https://www.youtube.com/watch?v=6ULyxuHKxg8&pp=ygUWYnl0ZWJ5dGVnbyBhcGkgZ2F0ZXdheQ%3D%3D) | [Notes](API_Gateway.md)
| Choosing right db | [Choose Right](https://www.youtube.com/watch?v=kkeFE6iRfMM&pp=ygUaYnl0ZWJ5dGVnbyBjaG9vc2UgcmlnaHQgZGI%3D) | [Notes](ChoosingRightDB.md)
| CI/CD | [CI/CD](https://www.youtube.com/watch?v=42UP1fxi2SY&pp=ygUQYnl0ZWJ5dGVnbyBjaSBjZA%3D%3D) | [Notes](CI_CD.md)
