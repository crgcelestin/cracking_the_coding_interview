# [Choosing Right DB Video](https://www.youtube.com/watch?v=kkeFE6iRfMM)

## Initial DB Questions
- Are we positive we need a new DB?
- Is the existing db breaking at seams?
- P95 Latency becoming an issue? Working set is overflowing available memory?
- Most basic requests need to go to disk and slow down rate of operation?

### Regarding Moving from Old to New DB Choice
- Can be a config knob that if mod'ed allows for increased breathing room
    * ^ can be convenient as migrating a db can take a sustained period of time
    * knobs (modifiable parameters): working set memory size, compaction strategy, or garbage collection behavior
- Can we put cache in front of db allowing for increased runway?
- Can read replicas be added to offload read load?
- Can we shard or partition data in some way?

## For new DBS
- In order to understand potential tradeoffs it is critical to identify stated limits and faqs
    * critical design constraints
- Many noSQL dbs allege higher scale vs traditional relational i.e near linear horizontal scalability
    * common tradeoffs: eliminate/limit transactional guarantees, limit data modeling flexibility
- after narrowing down db options, create realistic test bench using own data and measure p99
