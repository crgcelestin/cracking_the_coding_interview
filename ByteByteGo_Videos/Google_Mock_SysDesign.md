# [Mock Interview](https://www.youtube.com/watch?v=S1DvEdR0iUo)

> - __Question__:
>   - Given an api to track when a user has listened to a certain song, build a system that tracks top 10 most listened to songs and albums in these periods: last 1 week / 1 month / 1 year by user / country / globally

## Approach
1. Ask Clarifying Questions
    - *Use Cases*
        * API (top N songs/albums)
        * no personalization and no end user customization
    - *Scale*
        * 400M users with 40% activity
        * 2hr/day avg song listening rate
        * 70M songs with new songs being added with 60k/day
    - *Latency*
        * Not real time
        * NA, Europe having more users
        * Every hour refresh in certain popular regions regarding song storage, up to 1 day in less popular regions, with the option of it being configurable
    - *Query Latency*
        * 200qps to start
        * traffic may grow to 1000qps
    - *Data Calculations*
        * 200m -> 3hr a day -> 36 songs by user -> 200m*36/12
        * input qps is 600m songs/day
        * one event per song

2. Diagramming
    * ![Diag](./images/Diagram.png)
        - Box on right is black box that will be detailed

    - __Tracking api obtains data and then needs queue to perform storage__

        - Need to offload during peak times to avoid data loss to scale api

        * Choice of queue = **(Kafka)**
            - Known to be distributed, scalable
            - New event is captured and lands in K Queue

        * Streaming job reads from Kafka and copies it to a batch store
            - Batch system with max latency of 1 hr
            - Batch job is read from Kafka topic and written to batch store **(HDFS)**
                - HDFS is distributed, replicated data and scales well for batch processing
                - 1 partition/date
                - in order to avoid conflict of small files, append to date directory
                - Storing large data in 1 partition will have consequence of lesser performance on queries (which is negligible)

        * **(ETL)** pipeline processes raw events (per song listening event)
            - Provide schema to data
            ```py
            class Song(Base):
                album_id
                song_id
                user_id
                location
                app_identifier or app_session
            ```
            - schema put on data/date and written to song events -> songEvents and etl

            - metadata is available to HDFS, unless a few 100 records which can be pulled as part of batch processing

            - songs and albums involve millions of records that will be replicated to an offline data store
            - **(OLTP)** to offline data store either through changelog/change data capture/events like is done w/ Kafka
            - then to batch systems like HDFS w/ **(ETL)** performing joins and write songEvents

            * Enriched Schema
            ```py
            class Song(Base):
                timestamp
                    date
                    hr
                user_id
                    gender
                    age_group
                loåcation
                    city
                    region
                song_id
                    author
                album
                    genre
                <other dimensions regarding api credentials >
                app_identifier or app_session
            ```

            - Data Analytics
                -  **(BigQuery)** good for data analytics, or **(Presto)**
                - When a query occurs, aggregation occurs at op time = decent compute power consideration
                - perform aggregations -> etl at query occurrence -> other db where writes occur [ rdbms (top metrics table) ]

            - Reliability
                - When a system does go down, how does design cope?
                    - Tracking API
                        * Deployment in several regions such as 1 and 2 running concurrently
                        * Load Balancer can decide which one is required and flows as a response to the desired output region
                    - 100 no data loss and 100% data processing
                    - Kafka Compromise
                        - With both of regions considered, 1 region is down other region cam pick up slack
                        - add aggregated data layer which reads from Kafka and ensures data retention
                            - data is replicated in both regions
                            - batch processing only in region 1
                            - region 2 kept as a backup
                            - RDBMS in both regions

            - Health Checks
                - Implementation of monitoring alerts
                - Data health check which is monitoring
                - correct alerts address and notify in regards to data quality
                    - data is processed properly in correct format, checks and balances in pipelines, restart system -> recover to normal state

## Closing
- Proper Process
 1. gather requirements (high reqs, assumptions)
 2. perform design process
 3. time management (spend right time in low level comps)
 4. explain thought process frequently
    - listen to feedback and change courses if needed
 5. don't increase scope, only prob scope and keep course to salient points
 6. perform tarde off analysis (how do you arrive at optimal?)
 7. cover design aspects
    - ensure system is scalable, highly scalable, secure, conservative, realistic in resource expectations and easy to operate and maintain
