# What is Kubernetes?
- Open source container orchestration platform that automates deployment, scaling, and management of containerized apps
    - related to Google Borg, managing deployment of thousands of apps
    - Open source Borg
    - k8s, 8 = ubernete

- K8s cluster = set of machines known as nodes used to run containerized apps
    - 2 core pieces
        1. Control Plane
            * Responsible for managing cluster state
            * in prod environments, it runs on several nodes spanning several data center zones
                - API Server
                    * primary interface between control plane and rest of cluster
                    * exposes restful api allowing clients to interact with control plane and submit requests for cluster management
                - Controller Manager
                    * responsible for running controllers managing cluster state
                        - replication controller
                            * ensures that desired number of replicas of pod are running
                        - deployment controller
                            * managing rolling updates, rollbacks of deployments
                - Scheduler
                    - responsible for scheduling pods onto worker nodes in cluster
                    - use info about resources req by pods, available resources on worker nodes to make placement decisions
                - etcd
                    * used by api server, other comps of control plane to store, retrieve info about cluster
                    - distributed key value store
                    - stores cluster's persistent state
        2. Worker Nodes
            * Run containerized application workloads with container apps running in a Pod
                - Pods = smallest deployable units in K8s that are managed by control plane and are building blocks
                    - 1 or more containers
                    - Provide shared storage and networking
                - core comps of k8s running on worker nodes include kubelet, container runtime, kube proxy
                    * kubelet : daemon runs on each worker node
                        - resp for communicating with control plane and receives instructions from control plane about which pods to run on node
                        - ensures that desired state of pods is maintained
                    * container runtime runs containers on worker nodes
                        - resp for pulling container images from registry, starting and stopping containers, managing resources
                    * kube proxy - network proxy running on each worker node and resp for routing traffic to correct pods
                        * provides load balancing for pods, ensure that traffic is evenly distributed across pods

- When to use K8s
    - Upsides
        * scalable, highly available
        * fts: self-healing, automatic rollbacks, horizontal scaling
            - allows for easy scaling up and down of apps as needed (good for responding to demand)
        * portable - deploy and manage in consistent and reliable method
            - runs on premis, public cloud, hybrid
        * provides uniform way to package, deploy, and manage apps
    - Cons
        * complex to setup and operate
        * upfront cost is high, especially for orgs new to container orchestration
        * req high level of resource and orchestration
        * requires certain min level of resources to support all fts
        * one popular for balance - offload management of control point (eks, gke, aks on azure)
            -  take care of tasks required deep expertise
        * req for much larger orgs
