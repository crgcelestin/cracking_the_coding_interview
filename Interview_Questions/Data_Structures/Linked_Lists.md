# Arrays and Strings
## Hash Tables
- DS that maps keys to values for efficient lookup. In a simple implementation, use arary of linked lists and hash code function
    * Key Insertion:
        1. Compute key's hash code that is usually int/long
## ArrayList and Resizable Arrays
### Interview Questions
1. Implement algo to determine if string has only unique chars
2. Given 2 strings, determine method to decide if one is a permutation of other
3. Write method to replace string spaces with '%20'
    - Assume string has sufficient space to hold more chars
    - Given the 'true' string length
4. Write a function to determine if given a palindrome permutation
5. Only 3 edits possible on strings: insert, remove, replace char
    - Given 2 strings, write a function to determine if 1 or zero edits away
6. String Compression
    - Implement method to perform string compression using counts of repeated chars
    - `aabcccccaaa would become a2blc5a3`
    - if compressed string can't become smaller than original, method returns original
    - assume only uppercase, lowercase letters
7. Given image represented by N*M matrix where a pixel is 4 bytes, write method to rotate image by 90 degrees. Can it be done in place?
8. Write an algo that if an element in M*N matrix is 0, entire row and column are set to 0.
9. Assume you have method `isSubstring` that checks if one word = substring of another
    - Given 2 strings, `s1`, `s2`, write code to check if `s2` is a rotation of `s1` using only 1 call to `isSubstring`
