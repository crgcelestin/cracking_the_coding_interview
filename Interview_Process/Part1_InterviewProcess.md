# Part 1
- Algorithms and coding problems form largest component of interview process
    * Interviewer makes assessment regarding performance:
        1. Analytical Skills:
            - Did I need aid to solve problem? Solution optimization?
            - How long till I arrived at solution?
            - If I had to design/architect new solution, did I structure problem well, think of tradeoffs?
        2. Coding skills:
            - Able to successfully translate algo to reasonable code that is clean and well organized?
            - Did i think about errors and think about a good style?
            - Have you made good technical decisions in past?
        3. Tech knowledge/comp sci fundamentals:
            - Do I have strong foundation in comp sci, relevant technologies?
        4. Experience:
            - Have I made good technical decisions in past? - Have I built interesting, challenging projects?
            - Have I shown drive, initiative, other critical factors?
        5. Culture Fit/ Communication:
            - Do my personality and values fit in company and team?
            - Did i communicate well with interviewer?
    * weighing of each indv area depends on teh question, interviewer, role, team, company

- Why?
    - understand why questions are being asked
- understanding of basics
    * periodic understanding of trees, graphs, lists, sorting required
    * good proxy:
        - majority of problem solving questions involve basics
        - whiteboards encourage more communication, explain thought process
- relative evaluation
    * interviewer gains feel for performance by comparing to other people
