# Part 2: Behind the Scenes
- screening interview can often involve coding, algo questions with a bar that is as high for in-person interviews, ask the type from recruiter
- typically 3 tp 6 in person interviews
    * good time to discuss interests, company culture
- after interview, gain feedback and there will be recommendations/opinions submitted to a hiring committee

## MS Interview
* short interview w/ recruiter (provides sample question) or asked basic technical questions -> then during day, 4 to 5 interviews, with 2 various teams (get a feel for team culture)
* depending on team, interviewers may/may not share feedback with rest of interview loop
### Questions
- why microsoft:
- what's unique:

## Amazon Interview
- phone screen w/ specific team -> engineer who interviews ask for simple code to be written then broad set of questions to explore what areas of tech one is familiar with -> then to office, interview with teams that have selected based on resume, interviews
    * includes bar raiser interviewer (may struggle and is relative compared to others)
### Prep, Difference:
- Amazon cares about scale: view recs in System Design + Scalability
- asks a # of questions about OOD: review ood section
- Need to impress bar raiser and hiring manager

## Google Interview
- google engineer performs first phone screen with tough tech questions
- on site: 4 to 6 people (ask honest questions for lunch interviewer)
- no specific structure/system
 * feedback given through 4 category breakdown: analytical ability, coding, experience, communication
 * need to see one superb endorser among all averages
### Prep, Difference:
- as web company, google cares about scalability (refer to sys design, scalability section)
- strong focus on analytical/algo skills
- interviewers -> hiring comm for decision

## Apple Interview:
- min bureaucracy with interviewers looking for great tech skills and passion for position, company
- recruiter phone screen -> series of tech phone screens
- on campus: recruiter provides process overview -> 6 to 8 interviews with team members
- expect: mix of 1v1, 2v1 interviews
    * focus on differing areas based on interviewer
- if viable candidate: interview with director and org VP then decision
### Prep, Difference:
- if interviewing with specific team, read on product
    * what is liked about product? what to improve? offer specific recommendations

## Facebook Interview:
- 1 to 2 phone screens(ps), ps are technical, involving coding
- after ps, perform hw assignment including mix of coding + algos -> get someone to review code
- on site: each interviewer has role:
    * behavioral(1): assess ability to be capable in fb envi
        - asked about: culture, values, what i am excited about, how one tackles challenges, talk about fb interest
        - may be asked coding qs
    * coding, algos(2): designed for challenge
    * design/architecture(1): be swe will be asked sys design questions -> fe / other specialties: discuss solutions, tradeoffs
### Prep, Difference:
- show that one loves to build fast
- hack together elegant, scalable solutions with desired language of choice
- fb performs be work in c++, python, erlang, other languages
- will be 6-wk camp to ramp in code base
    * includes: mentorship, learn best practices, increased flexibility in choosing a project

## Palantir Interview:
- interviews for a specific team, app may be routed to other team with greater fit
- start w/ 2 ps that are 30-45 min in length and primarily technical, with heavy tech focus
- successful candidates are invite to campus and interview with up to 5 people + ask god questions, demo passion for company
### Prep, Difference:
- qs harder than those at other top companies, pref towards increased challenging qs
- learn core ds and algos inside, out -> focus with hardest algo questions
- sys design req for be role

# 3 - skipped as it is specific to certain cases
# 4 - Before Interview
- Employment History
    - include relative positions, w/ each bullet point:
        * discuss accomplishments with this approach:
            - 'accomplished x by implementing y which led to z'
                * Reduced object rendering time by 75% by implementing distributed caching, leading to a 10% reduc­tion in log-in time
                * Increased average match accuracy from 1.2 to 1.5 by implementing a new comparison algorithm based on windiff
            - what is done -> how it was done -> results (measurable)
- Projects
    * 2-4 more significant projects
        - what project was, languages/tech employed, individual/team project
        - open source, indep projects
- Programming Languages, Software
    * add experience level next to given languages
# 5 - Behavioral
- Weakness
    * convey real, legit weakness and emphasize how one overcame it
- Qs to ask Interviewer
    * Genuine
        - What is ratio of testers to developers to program managers? What is the interaction?
        - How does project planning occur on team?
        - What brought you to company? What has been most challenging?
    * Insightful
        - I noticed you use tech X. How do you handle problem Y?
        - why did the product choose x protocol over y protocol? i know it has benefits like a,b but many companies choose to not use it due to issue d.
    * Passion
        - 'interested in scalability. What opps are available to learn?'
- know tech projects
    * project had challenging comps, played a central role, talk at tech depth
        - talk about challenges, mistakes, tech decisions, tech decisions and tradeoffs, actions to be done differently
- behavioral:
    * limit details - stay light on details and state key points
        - ex: By examining the most common user behavior and applying the Rabin-Karp algorithm, I designed a new algorithm to reduce search from O(n) to 0(log n) in 90% of cases. I can go into more details if you'd like.
    * focus on self
    * structured answers:
        - Nugget First means starting your response with a "nugget" that succinctly describes what your response will be about.
            * For example:
                * Interviewer: "Tell me about a time you had to persuade a group of people to make a big change:'
                * Candidate: " Sure, let me tell you about the time when I convinced my school to let undergraduates teach their own courses. Initially, my school had a rule where..:'
        - Use STAR approach in order to outline stories for questions like 'tell me about challenging interaction with a teammate'
        - ideal stories should exemplify:
            * initiative/leadership, empathy, compassion, humility, teamwork/helpfulness
    * structure
        - current role
        - college
        - post college, onwards
        - current role [details]
        - outside of work
        - wrap up
            * include shows of successes
