# Cracking The Coding Interview

- The infamous book, pushing my progress of CTCI
- ByteByteGo System Design [Guide](./ByteByteGo_Videos/Guide.md)

|  Section | Section Notes |
| - | - |
| Interview Process | [Intro](/Interview_Process/Part1_InterviewProcess.md) |
| ... | [Behind Scenes](/Interview_Process/Part2,4,5_BehindScenes.md) |
| Big O | [Big O](/Big_O/BigO.md) |
... (tbd)
