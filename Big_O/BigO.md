# Section 6 - Big O
- language and metric used to describe algo efficiency
- allows for understanding of algo being faster or slower
## Analogy
- Easier to commute with a 1tb amount of hard drive of data then to send it via email, online protocol
## TC
- algo runtime:
    * e-transfer -
        - O(s) where s = file size, time to transfer increases linearly with file size
    * plane-transfer -
        - o(1), file size does not relate to the duration to commute with said file
    * regardless of how big constant is, how slow linear increase is, linear will surpass constant eventually
        - common: O(log N), O(n log n), O(n), O(n^2), O(2^n)
            - can have several variables in runtime
    * Big O = upper bound on time - algo prints values in array described as O(n)... < - tightest runtime description
    * For algo analysis, always use Worst Case
        - ex: quick sort - pivot is repeatedly biggest array element [ if pivot is chosen to be 1st element in subarray, array is sorted in rev order ]
            * shrink subarray by one element -> O(n^2) runtime


## SC
* Amount of memory/space req by algorithm
    - parallel concept to tc [ array of size n, req n space ]
    - 1 d array req n space, 2d array of (n*n) require N^2 space

#### Examples
```c
// recursive call counts
int sum(int n){
    if(n<=0){
        return 0;
    }
    return n+sum(n-1)
}
// each call adds level to stack, takes up memory -> worst case: O(n) time and space complexity
```

```c
// add adjacent elements between 0 and n
int pairSumSeq(int n){
    int sum = 0;
    for(int i=0; i<n; i++){
        sum+=pairSum(i, i+1)
    }
    return sum;
}
int pairSum(int a, int b){
    return a+b;
}
// O(n) tc, O(1) sc
```
## Drop Constants
- Possible for O(N) code to run > than O(1) code for certain inputs
    * Big O describes rate of increase
    - drop constants in runtime, algo describes as O(2N) is actually O(N)
        * O(N) is not always better than O(N^2)

## Drop Non-Dominant Terms
- ![Big O](BIgO.png)
    * terms with increasing steps (y values) proportional to time (x values) dominate
        * O(N^2+N) = O(N^2)
        * O(N+log N) = O(N)
        * O(5*2^N + 1000N^100) = O(2^N)

## Multi Part Algos
```c
// add runtimes O(A+B)
for(int a: arrA){
    print(a);
}
for(int b: arrB){
    print(b)
}
/*
    perform A then B
    total amt of work is O(A+B)
*/

// multiply runtimes O(A*B)
for(int a: arrA){
    for(int b: arrB){
        print(a + "," + b);
    }
}
/*
    perform B for every A performed
    O(A*B)
*/
```
## Amortized Time
- In ArrayList or dynamically resizing array, have the benefits of defined array ds while offering size flexibility (capacity grows with insertion -> linear O(n))
    * implemented with an array, when it hits capacity -> new array is created with double capacity, copy elements to new array
    * Vast majority - time insertion is O(1) time
- Array Insertion: as elements are inserted, capacity is continued to be doubled -> x + x/2 + x/4 -> 1 (2X), with amortized being O(1)

## Log N Runtimes
- O(log N) is common in runtimes
    * Binary search - looking for x in an N-element sorted array, if x is less than middle then search on left side of array, if x is greater than middle then search on right side of array
        - during said process, shifting two pointers during process to refine range of #s
    * 2^k = N -> log_2(N) = k runtime
- if the number of elements in problem is continuing to be 1/2ed then the runtime is likely log n
    * finding element in balanced binary tree (depending on comparison go either to left or right)

## Recursive Runtimes
```c
int f(int n){
    if(n<=1){
        return 1
    }
    return f(n-1) + f(n-1);
}
```
- in this example, tree will have depth of n and each node has n children and thus a tc of 2^n [ O(2^n) ]
    * f(4) calls f(3) twice which results in f(2) being called 4 times then f(1) is called 8 times
- With recursive functions, there will be a rule of O(branches^depth), where branches = # of times recursive call branches
- For exponential functions, space complexity is O(n) as there O(n) will exist at any given time

## Examples and Exercises
[Notes](ExamplesExercises.md)
## Additional Problems
[Notes](AdditionalProblems.md)
