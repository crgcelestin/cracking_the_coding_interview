# Examples and Exercises
- Example 1
```java
void foo(int[] array){
    int sum = 0;
    int product = 1;
    for(int i=0; i<array.length; i++){
        sum+=array[i];
    }
    for(int i=0; i<array.length; i++){
        product*=array[i]
    }
}
// takes O(N) time
// assuming each for loop as it relates to steps occurs simultaneously
```

- Example 2
```java
void printPairs(int[] array){
    for(int i=0; i<array.length; i++){
        for(int j=0; j<array.length; j++){
            System.out.println(array[i] + "," + array[j])
        }
    }
}
// inner for loop has O(n) iterations and is called N times thus O(n^2)
```

- Example 3
```java
void printUnorderedPairs(int[] array){
    for(int i = 0; i< array.length; i++){
        for(int j=i+1; j<array.length; j++){
            System.out.println(array[i] "," + array[j])
        }
    }
}
// sum of 1 through n-1 is N((N-1)/2) -> O(N^2)
/*
    the reason its N(N-1)/2 is because j runs for N-1 -> N-2 -> N-3 steps, etc
*/

// Alternative thinking
/*
    figure out runtime visually by understanding code intention, iterate through each pair of values for (i,j) where j is larger than i by an integer of 1

    There are N^2 total pairs, 1/2 going to have i<j, other 1/2 will have i>j
        Code goes through N^2/2 pairs for O(N^2)
    When outputting, it looks like 1/2 of N*N matrix of size N^2/2 -> O(N^2)
*/
```

- Example 4
```java
void printUnorderedPairs(int[] arrayA, int[] arrayB){
    for(int i = 0; i< arrayA.length; i++){
        for(int j=0; j<arrayB.length; j++){
            // O(1) work as it is a constant time statement
            // if the value in arrayA at pointer i is less than that of the values in arrayB at pointer j
            if (arrayA[i]<arrayB[j]){
                System.out.println(arrayA[i] "," + arrayB[j])
            }
        }
    }
}

// runtime has to be O(ab) where a=arrayA.length, b=arrayB.length, not O(N^2) as there are two different loops dependent on two separate arrays of varying length
```

- Example 5
```java
void printUnorderedPairs(int[] arrayA, int[] arrayB){
    for(int i = 0; i< arrayA.length; i++){
        for(int j=0; j<arrayB.length; j++){
            // constant work
            for(int k=0; k<1000; k++){
                System.out.println(arrayA[i]+","+arrayB[j]);
            }
        }
    }
}

// Runtime still O(ab) due to k<1000 i.e a constant constraint
```

- Example 6
```java
void reverse(int[] array){
    for(int i=0;i<array.length/2;i++){
        // ex: starting at first element (0), with array of 7 -> 6 = 7 - 0 - 1
        int other = array.length - i - 1
        // ex: store element with temp
        int temp = array[i]
        // ex: swap element at end to the current start index
        array[i] = array[sother]
        // ex: change element at end to element at start
        array[other]=temp;
    }
}
// runtime in O(n) time as it reverses an entire array
```

- Example 7
    * O(N+P), P<N/2
        - if `p<n/2`, then N is dominant term
    * O(2N)
        - drop constant, trends linear
    * O(N + log N)
        - drop logN, just O(N) linear
    * O(N+M)
        - no established relation between N and M, keep both vars
        - still O(N+M)

- Example 8
    * if there is an algo, take in array of strings -> sort each string -> sort full array
        1. let s be length of longest string
        2. let a be array length
        - in parts
            1. sorting each string is O(s log s)
            2. perform this for every string -> O(a * s log s)
            3. now to sort all strings
                - Need to compare strings with each taking O(s) time, with O(a log a) comparisons thus O(a*s log a) time
    * end if summed is O(as(log a + log s))

- Example 9
```java
int sum(Node node){
    if(node==null){
        return 0;
    }
    return sum(node.left) + node.value + sum(node.right)
}
/*
    What It Means
        - code touches on each node in tree once and performs constant time amt of work with each 'touch' (excluding recursive calls)
        - runtime is linear in terms of number of nodes -> if N nodes, runtime = O(N)

    Recursive Pattern
        - Runtime of recursive function with several branches is typically O(branches^depth)
            * as there are two branches in this example it is O(2^depth)
            * depth in this case is the runtime of performing binary search on balanced BST, if n total nodes -> depth = log N
            * thus we obtain O(2^log N)
        - log_2 means 2^P = Q -> log_2(Q) = P
        * P = 2^(log N) -> log_2(P) = log_2(N) -> P = N
            - thus 2^(log N)= N thus a runtime of O(N), N being number of nodes
            - logarithmic identity of a^(log_a(x)) = x
*/
```

- Example 10
    * method checks if # is prime based on numbers < than it. Only needs to go up to square root of n as if n is div by # greater than square root, it div by something smaller

```java
boolean isPrime(int n){
    for(int x=2; x*x <=n; x++){
        if(n%x==0){
            return false
        }
    }
    return true
}
/*
    work inside for loop is constant and we need to know the number of iterations for loop in worst case
        - for loop starts when x=2, ends when x * x = n, stops when x=sqrt(n), thus it runs in O(sqrt(N)) as loop stops when x<=sqrt(n)
*/
```

- Example 11
```java
// computs n!
int factorial(int n) {
    if (n < 0) { return -1; }
    else if (n 0) { return 1; }
    else { return n * factorial(n - 1); }
}
/*
    straight forward recursion with 1 branch occurring from n -> n-1 ... -> 1 thus O(n) time
*/
```

- Example 12
```java
void permutation(String str) {
     permutation(str, "");
}

void permutation(String str, String prefix) {
     if (str.length() == 0) {
    7   System.out.println(prefix);
    } else {
    9   for (int i= 0; i < str.length(); i++) {
            String rem = str.substring(0, i) + str.substring(i + 1);
            permutation(rem, prefix + str.charAt(i));
            }
    12 }
}
/*
    (start) think about the number of times permutation is called, how long each call takes

    how many times is the permutation called in base case?
        - if we were to generate a permutation, need to pick chars for each 'slot', in 1st slot we have 7 choices -> once we pick letter there, we have 6 choices for next slot then 5 -> etc.
        - thus n! permutations, permutation is called n! times

    how many times does permutation get called before base case?
        - consider number of times lines 9-12 are hit, there will be no more than n * n! nodes (function calls) in tree

    how long does each function call take?
        - execution of line 7 is O(n) as each char needs to be printed
        - line 10 and 11, take O(n) combined due to str concentration -> sum of lengths of rem, prefix, str.charAt(i) are always n
        - each node of call tree corresponds to O(n) work

    what is the total runtime?
        - calling permutation O(n*n!) times (as upper bound) and each one takes O(n) time, total runtime does not exceed O(n^2 * n!)
*/
```

- Example 13
```java
int fib(int n) {
    2 if (n <= 0) return 0;
    3 else if (n == 1) return 1;
    4 return fib(n - 1) + fib(n - 2);
    }
/*
    use the pattern O(branches ^ depth)
    2  branches per call so 2^n
    generally exponential runtime for several recursive calls
*/
```
- Example 14
```java
void allFib(int n) {
    for (int i= 0; i < n; i++) {
        System.out.println(i + " : "+ fib(i));
    }
    int fib(int n) {
        if (n <= 0) return 0;
        else if (n == 1) return 1;
        return fib(n - 1) + fib(n -2);
    }
}
/*
    fib(n) takes O(2^n) time and is called n times
    n is changing throughout meaning fib(1) -> 2^1 steps / fib(2) -> 2^2 steps ... fib(n) -> 2^n steps
    meaning total work is 2^1 + ... + 2^n -> 2^n+1 (runtime to compute is still 2^n)
*/
```
- Example 15
* prints all fib #s from 0 to n, this time previous computed values in integer array are cached -> if already done, just return cache
```java
void allFib(int n) {
    int[] memo = new int[n + 1];
    for (int i= 0; i < n; i++) {
        System.out.println(i + " : "+ fib(i, memo));
    }
}

int fib(int n, int[] memo){
    if (n =< 0) return 0;
    else if (n == 1) return 1;
    else if (memo[n] > 0) return memo[n];
    memo[n] = fib(n - 1, memo)+ fib(n - 2, memo);
    return memo[n];
}
```
- Algorithm Walkthrough:
```md
fib(l) -> return 1

fib(2)
fib(l) -> return 1
fib(0) -> return 0 store 1 at memo[2]

fib(3)
fib(2) -> lookup memo[2]
fib(l) -> return 1
store 2 at memo[3]

fib(4)
fib(3) -> lookup memo[3]
fib(2) -> lookup memo[2]
store 3 at memo[4]

fib(S)
fib(4) -> lookup memo[4] -> return 3
fib(3) -> lookup memo[3] -> return 2
store 5 at memo[S]
```
* at each call to fib(i), we computed and stored values for fib(i-1) and fib(i-2), look up values -> sum them -> store new result -> return [ takes constant time being O(1)]
    - concept of memoization is common to optimize exp time recursive algos


- Example 16
```java
int powers0f2(int n) {
    if (n < 1) {
        return 0;
    } else if (n == 1) {
        System.out.println(l);
        return 1;
    } else {
        int prev = powers0f2(n / 2);
        int curr = prev * 2;
        System.out.println(curr);
    return curr;
    }
}
```
* function prints powers of 2 from 1 through n inclusive, if n=4 -> print 1,2,4 (2^0, 2^1, 2^2)
* The runtime is thus the number of times we can divide n/2 -> 1 meaning it has a runtime of O(log n)

### What it Means
- Approach runtime by attempting to understand what process the code is performing -> each call to powersOf2 leads to one number printed and returned -> if algo prints 13 values then powersOf2 are called 13 times
- as printing occurs with all powers of 2 between 1 and n, number of times function is called = powers of 2 between 1 and n
    * there are logN powers of 2 between 1 and n, thus O(log n)

### Rate of Increase
- final way to approach runtime: how does runtime change as n becomes bigger?
    * if n goes from P -> P+1, number of calls to powersOfTwo may not change, calls increase by 1 each time n doubles in size
    * each time n doubles, calls increases by 1, 2^x = n, x = log_2(n)

## Additional Problem
```java
// v1.1
int product(int a, int b) {
    int sum = 0; for (int i= 0; i < b; i++){ sum += a; }
    return sum;
    }
// O(b) constant time as for loop is constrained by constant integer b

// v1.2
// typical rule for recursive fxns is that the runtime is O(branches^depth)
int power(int a, int b){
    if (b < 0) {
        return 0;
    } else if (b == 0) {
        return 1;
    } else {
    return a * power(a, b - 1);
    }
    }
// 1 branch, however a and b are both constant, so were are restrained to O(1^(a*b))
// correction: O(b), code iterates through b calls as it subtracts one at each level

// v1.3
int mod(int a, int b) {
    if (b <= 0) {
        return -1;
    }
    int div = a / b;
    return a - div * b;
}
// O(1) as the fxn performs one operation regardless of size of input

// v1.4
int div(int a, int b) {
    int count = 0;
    int sum = b;
    // sum = a/b <- how many times can b be added to sum to get to a sum/b = a
    while (sum <= a) {
        sum += b;
        count++
    }
    return count;
}
// sum+b -> a, count is constrained by upper bound of constant thus O(a/b), while loop iterates count times which approximates a/b

// v1.5
/*
    computes integer sqrt of number, if number is not perfect square then return -1 -> performs via successive guessing
    if n is 100, first guess is 50, if too high -> go lower (1/2 between 1 and 50) and vice versa
*/
int sqrt(int n) {
    return sqrt_helper(n, 1, n);
}

int sqrt_helper(int n, int min, int max) {
    if (max < min) return -1; // no square root
    int guess = (min + max) I 2;
    // sqrt(n) = guess
    if (guess *guess == n) {// found it!
        return guess;
    } else if (guess *guess < n) {
        //too low
        return sqrt_helper(n, guess + 1, max); //try higher
    } else {
        //too high
        return sqrt_helper(n, min, guess - l) ; //try lower
    }
}
// going off RR, potentially O(log n)
/*
    loop is performing a binary search for a square root
    visualizing a tree we are making the choice between the next largest integer or next smallest for guessing
*/

// v1.6
int sqrt(int n) {       // guess <= sqrt(n)
    for (int guess = 1; guess * guess <= n; guess++) {
         if (guess * guess == n){
                return guess;
        }
    }
return -1;
}
// O(sqrt(n)
```
- V 1.7
    * If a binary search tree is not balanced, how long might it take (worst case) to find an element in it?
        - Worst case we have a tree that only has one branch that consists of n elements in a row and we are searching for a node that is the last of said branch's nodes
        - thus we have to iterate over n nodes, O(n) as compared to O(log n)
- V 1.8
    * looking for a specific value in a binary tree, but tree is not a binary search tree -> what is it's time complexity?
        - O(n), alternative trees can have more than 2 children per node, search can be increased as number of nodes increases -> linear as we will have to search through all nodes

```java
// v 1.9
/*
    appendToNew method appends value to array to create new, longer array and returns new longer array
    Used appendToNew method to create copyArray function that repeatedly calls appendToNew - How long does copying an array take?
*/

int[] copyArray(int[] array) {
    int[] copy= new int[0];
    for (int value : array) {
        copy= appendToNew(copy, value);
    }
    return copy;
}

int[] appendToNew(int[] array, int value) {
    // copy all elements over to new array
    int[] bigger= new int[array.length + 1];
    for (int i= 0; i < array.length; i++) {
        bigger[i] = array[i];
    }
    // add new element
    bigger[bigger.length - 1] = value;
    return bigger
}
/*
copying elements to a bigger array from previous means iterating through each element and then copying it to new array, access in an indexed array is O(1) doing that n times, then inserting in new array is O(n*n)
*/

// v 1.10
// sums digits in a number, what is its big O time?
int sumDigits(int n) {
    int sum = 0;
    while (n > 0) {
        sum += n % 10;
        n /= 10;

}
return sum;
}

/*
    iterate through integers in said number, what determines how long this process will take depends on length of number so O(log(n)?)
    Runtime is number of digits in number
        number with d digits can have a value up to 10^d
        if n=10^d, then d = log n
*/

// v1.11
/*
    prints all strings of length k where chars are in sorted order
    Performs this by: generating all strings of length K and check if each is sorted
*/
int numChars = 26;

void printSortedStrings(int remaining) {
    printSortedStrings(remaining, "");
}

void printSortedStrings(int remaining, String prefix) {
    if (remaining== 0) {
        if (isinOrder(prefix)) {
            System.out.println(prefix);
        }
    } else {
        for (int i= 0; i < numchars; i++) {
            char c = ithletter(i);
            printSortedStrings(remaining - 1, prefix + c); }
    }
}

boolean isinOrder(String s) {
    for (int i= 1; i < s.length(); i++) {
        int prev ithLetter(s.charAt(i - 1));
        int curr = ithLetter(s.charAt(i));
        if (prev > curr) {
            return false;
        }
    }
    return true;
}

char ithLetter(int i) {
    return (char) (((int) 'a') + i);
}
/*

    sorting takes O(N) comparisons in a given array and we need to iterate through an array of strings to only find strings of length k

    solution: k * c^k
    k  = length of string
    c = number of chars in alphabet (26?), takes O(c^k) to generate each string
    Need to check if each is sorted, taking O(k) time
    O(k * c^k)
*/


// v1.12
/*
    computes intersection (# of elements in common) of 2 arrays, assuming neither array has duplicates
    computes intersection by sorting 1st array and then iterate through 2nd checking w/ binary search if each values is in b
*/
int intersection(int[] a, int[] b) {
    mergesort(b);
    int intersect = 0;

    for (int x : a) {
        if (binarySearch(b, x) >= 0) {
            intersect++;
        }
    }
    return intersect;
}

/*
    binary search takes O(log b)
    sorting one array has to be O(n)
    so O(n log b)

    Correct: O( b log b + a log b)
    - sort array b takes O(b log b) time then for each element in a, perform binary search in O(log b) time -> 2nd part takes O(a log b) time
    thus total time is
    O( (a+b) * log b )
*/
```
